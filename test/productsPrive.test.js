const assert = require('chai').assert;
const expect = require('chai').expect;
var request = require("request");
const controller = require("../app/controllers/auth.controller");

describe('Test api products Prive', () => {
    var url = "http://localhost:8081/api/prive/produits";

    it("returns status 403 permission dined si l'user n'est pas connecté", function() {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(403);
        });
    });

});