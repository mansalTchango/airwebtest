const assert = require('chai').assert;
const expect = require('chai').expect;
var request = require("request");
const controller = require("../app/controllers/auth.controller");

describe('Test api products public', () => {
    var url = "http://localhost:8081/api/public/produits";

    it("returns status 200", function() {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
        });
    });

    it('Check si result api public/produits is array', () => {
        request(url, function(error, response, body) {
            var result = JSON.parse(body)
            assert.isArray(result);
        });
    });

    it('Check si visible_public = 1', () => {
        request(url, function(error, response, body) {
            var result = JSON.parse(body)
            result.map((item) => {
                expect(item.visible_public).to.equal(1);
            });
        });
    });

});