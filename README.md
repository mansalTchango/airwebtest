Airweb-test
===============

Projet de `test` Mansal Tchango pour airweb 

Création des tables de la base de données 
===============

Veuillez vous munir d'une base de données MySql.

1 - Create Database airwebtest
---------------
```
CREATE DATABASE airwebtest ;
```

2 - Create Table products
---------------
```
CREATE TABLE products
(
    id INT PRIMARY KEY NOT NULL,
    label VARCHAR(100),
    description VARCHAR(500),
    price INT,
    category_id VARCHAR(250),
    thumbnail_url VARCHAR(250),
    visible_public INT,
    visible_authenticated INT
);
```

3 - Create Table categories
---------------
```
CREATE TABLE categories
(
    id INT PRIMARY KEY NOT NULL,
    `index` INT,
    label VARCHAR(100),
    description VARCHAR(500)
);
```

4 - Create Table users
---------------
```
CREATE TABLE users
(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password_hash VARCHAR(100) NOT NULL,
    PRIMARY KEY (id, email) 
);

```

Insertion des données
===============

1 - Insert Table products
---------------
```
INSERT INTO products (id, label, description, price, category_id, thumbnail_url, visible_public, visible_authenticated)
 VALUES
 (1, 'Ticket public', 'Ticket disponible pour tous', 10, 1, 'https://www.itespresso.fr/wp-content/uploads/2016/05/showroomprive-france-billet-1280x720.jpg', 1, 1),
 (2, 'Abonnement public', 'Abonnement disponible pour tous', 100, 1, 'https://www.itespresso.fr/wp-content/uploads/2015/01/weezevent-vente-privee-1280x720.jpg', 1, 1),
 (3, 'Ticket privé', 'Ticket disponible pour tous', 10, 2, 'https://www.lagazettedescommunes.com/wp-content/uploads/2018/04/ticket-310x207.jpg', 0, 1),
 (4, 'Abonnement privé', 'Ticket disponible pour tous', 10, 2, 'https://b-esa.be/images/storychief/lukas-qs7fr07bljm-unsplash_506e1c965c7e680c11858946a12ef12e.jpg', 0, 1)

```

2 - Insert Table categories
---------------
```
INSERT INTO categories (id, `index`, label, description)
 VALUES
 (1, 1, 'public', 'produit disponible pour tous'),
 (2, 2, 'privé', 'produit privés')

```

3 - Insert Table users
---------------
```
INSERT INTO users (name, email, password_hash)
 VALUES
 ('user1', 'user1@gmail.com', 'bonjour'),
 ('user2', 'user2@gmail.com', 'bonjour'),
 ('user3', 'user3@gmail.com', 'bonjour')

```

Installation du serveur
===============

`Download source code` 

1 - run commande install node
---------------

`npm insatll` 

2 - Changer les variables d'environement
---------------

Changer les variables d'environement par rapport à votre bdd my sql dans le fichier `.env` 

3 - start le serveur
---------------

`npm start`

4 - run les tests unitaire de mocha
---------------

`npm test` (vous pouvez visualiser les resultats des tests dans le repertoir `test\reporte.html` )


