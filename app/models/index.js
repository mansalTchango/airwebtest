const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,

  dialectOptions: {
    socketPath: dbConfig.socketPath,
    supportBigNumbers: true,
    bigNumberStrings: true
  },

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.products = require("../models/products.model.js")(sequelize, Sequelize);
db.categories = require("../models/categories.model.js")(sequelize, Sequelize);
db.user = require("../models/user.model.js")(sequelize, Sequelize);

db.products.belongsToMany(db.categories, {
  through: "products_categories",
  foreignKey: "productsId",
  otherKey: "categoriesId"
});

db.categories.belongsToMany(db.products, {
  through: "products_categories",
  foreignKey: "categoriesId",
  otherKey: "productsId"
});

module.exports = db;