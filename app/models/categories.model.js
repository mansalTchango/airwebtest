module.exports = (sequelize, Sequelize) => {
    const Categories = sequelize.define("categories", {
        id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
        index:  { type: Sequelize.INTEGER },
        label: { type: Sequelize.STRING(100) },
        description: { type: Sequelize.STRING(500) }
    }, {
        timestamps: false
    });
  
    return Categories;
  };