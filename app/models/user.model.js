module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        id: { type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
        name:  { type: Sequelize.STRING(100), allowNull: false, },
        email: { type: Sequelize.STRING(100), allowNull: false, primaryKey: true, unique: true},
        password_hash: { type: Sequelize.STRING(100), allowNull: false, }
    }, {
        timestamps: false
    });
  
    return User;
  };