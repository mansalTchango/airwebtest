module.exports = (sequelize, Sequelize) => {
    const Products = sequelize.define("products", {
        id: { type: Sequelize.INTEGER, autoIncrement: false, primaryKey: true},
        label:  { type: Sequelize.STRING(100) },
        description: { type: Sequelize.STRING(500) },
        price: { type: Sequelize.INTEGER },
        category_id: { type: Sequelize.STRING(250) },
        thumbnail_url: { type: Sequelize.STRING(250) },
        visible_public: { type: Sequelize.INTEGER },
        visible_authenticated: { type: Sequelize.INTEGER },
    }, {
        timestamps: false
    });
  
    return Products;
  };