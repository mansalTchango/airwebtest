const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const md5 = require('md5')

var jwt = require("jsonwebtoken");

exports.signin = (req, res) => {
    User.findOne({
        where: {
        email: req.body.email
        }
    })
    .then(user => {
        if (!user) {
            return res.status(404).send({ message: "User Not found." });
        }

        var passwordIsValid  = md5(req.body.password) === md5(user.dataValues.password_hash)

        if (!passwordIsValid) {
            return res.status(401).send({
            accessToken: null,
            message: "Invalid Password!"
            });
        }


        var token = jwt.sign({ id: user.dataValues.id }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            id: user.dataValues.id,
            name: user.dataValues.name,
            email: user.dataValues.email,
            accessToken: token
        });
  
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
};