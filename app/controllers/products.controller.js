const db = require("../models");
const Products = db.products;

exports.productsPublic = (req, res) => {
    Products.findAll({
        where: {
            visible_public: 1
        }
    })
    .then(product => {
        if (!product) {
            return res.status(404).send({ message: "product Not found." });
        }

        var result = []

        product.forEach((item, index) => {
            result.push(item.dataValues) //value
        })

        res.status(200).send(result);
  
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.productsPrive = (req, res) => {
    Products.findAll({
        where: {
            visible_public: 0
        }
    })
    .then(product => {
        if (!product) {
            return res.status(404).send({ message: "product Not found." });
        }

        var result = []

        product.forEach((item, index) => {
            result.push(item.dataValues) //value
        })

        res.status(200).send(result);
  
    })
    .catch(err => {
        res.status(500).send({ message: err.message });
    });
};